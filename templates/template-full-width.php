<?php
/**
 * Template Name: Full Width Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_template_part( 'header' ); ?>


<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://res.cloudinary.com/supplychain-/raw/upload/v1660847881/supply%20chain/js/owl.carousel_jcz9m6.js"></script>
    <script src="https://res.cloudinary.com/supplychain-/raw/upload/v1661175856/supply%20chain/js/inline_vta9ny.js"></script>
    <link rel="stylesheet" href="https://res.cloudinary.com/supplychain-/raw/upload/v1660850184/supply%20chain/css/root_sqfuim.css">
    <link rel="stylesheet" href="https://res.cloudinary.com/supplychain-/raw/upload/v1661180508/supply%20chain/css/style_kguehp.css">
    <header>
        <a href="https://pulsehub.com.br/" class="logo mobile_logo">#raizentech</a>
        <div class="mobile-menu">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="containerNav">
            <nav class="nav-menu">
                <a href="https://pulsehub.com.br/" class="logo">#raizentech</a>
                <ul>
                    <li>
                        <a class="anchor" href="#inicio">Início</a>
                    </li>
        
                    <li>
                        <a class="anchor" href="#setores">Setores</a>
                    </li>
        
                    <li>
                        <a class="anchor" href="#oportunidades-e-regulamento">Oportunidades e Regulamento</a>
                    </li>
        
                    <li>
                        <a class="anchor" href="#cronograma">Cronograma</a>
                    </li>
                </ul>
        
                <a href="https://app.pipefy.com/public/form/yVidpha_" target="_blank" class="primaryButton">QUERO ME INSCREVER</a>
            </nav>
        </div>
    </header>
    <main>
		<section class="containerHidden">
			<div id="inicio" class="presentationContent">
				<div class="wrapperInformation">
					<p>Faça parte da Chamada e ajude a otimizar a cadeia de suprimentos da Raízen, líder global na transição energética.</p>
					<a href="https://app.pipefy.com/public/form/yVidpha_" target="_blank" class="primaryButton">Quero inscrever minha startup</a>

					<a href="#primaryAnchor" class="anchorMore">CONHEÇA MAIS</a>
				</div>

				<span class="graphics"></span>
			</div>
		
		
			<div id="primaryAnchor">
				<div class="containerCards mobileCarousel">

					<div class="cardBlock">
						<div class="maskImage">
							<img src="https://res.cloudinary.com/supplychain-/image/upload/v1660846799/supply%20chain/images/pulse-image_romdyk.png" data-src="https://res.cloudinary.com/supplychain-/image/upload/v1660846799/supply%20chain/images/pulse-image_romdyk.png" alt="pulse-image">
						</div>

						<div class="cardInformation">
							<h2>Sobre o Pulse</h2>
							<p>Com 5 anos de existência, o Pulse é um dos grandes hubs incentivadores de inovação aberta do Brasil, e conta com mais de 800 startups na base. Atua na viabilização de novas ideias e práticas que enriquecem o agronegócio brasileiro e demais frentes de negócios da Raízen, como logística, indústria e varejo.</p>
						</div>
					</div>

					<div class="cardBlock">
						<div class="maskImage">
							<img src="https://res.cloudinary.com/supplychain-/image/upload/v1661175052/supply%20chain/images/sobre-a-raizen2_m88p3f.jpg" data-src="https://res.cloudinary.com/supplychain-/image/upload/v1661175052/supply%20chain/images/sobre-a-raizen2_m88p3f.jpg" alt="pulse-image">
						</div>

						<div class="cardInformation">
							<h2>Sobre a Raízen</h2>
							<p>Empresa integrada de energia e referência global em biocombustíveis e bioeletricidade. É campeã global em renováveis e tem como compromisso produzir hoje a energia do futuro. Distribui combustíveis por meio de múltiplos modais para portos, aeroportos e clientes B2B, e conta com mais de 51 startups parceiras via Pulse.</p>
						</div>
					</div>

					<div class="cardBlock">
						<div class="maskImage">
							<img src="https://res.cloudinary.com/supplychain-/image/upload/v1661174429/supply%20chain/images/sobre-chamada_fokomw.jpg" data-src="https://res.cloudinary.com/supplychain-/image/upload/v1661174429/supply%20chain/images/sobre-chamada_fokomw.jpg" alt="pulse-image">
						</div>

						<div class="cardInformation">
							<h2>Sobre a Chamada</h2>
							<p>Buscamos a geração contínua de valor para os negócios da Raízen e para a sociedade através de novas tecnologias, soluções e parcerias. Buscamos startups e empresas de base tecnológica para transformar a cadeia de suprimentos, com o desenvolvimento de novos produtos, serviços e modelos de negócio.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

        <section id="setores" class="primaryContent">
            <div class="purpleContainer">
                <h2 class="textCenter">SETORES PROCURADOS PARA A CHAMADA</h2>
    
                <div class="primaryCarousel">
                                    
                    <div class="cardBlock">
                        <div class="maskImage">
                            <img src="https://res.cloudinary.com/supplychain-/image/upload/v1660846799/supply%20chain/images/orcamento_npohlj.png" data-src="https://res.cloudinary.com/supplychain-/image/upload/v1660846799/supply%20chain/images/orcamento_npohlj.png" alt="Gestão Orçamentária">
                        </div>
    
                        <div class="cardInformation">
                            <h2>Gestão Orçamentária</h2>
                            <p>Softwares ou soluções que suportem a atualização e acompanhamentos de índices de mercado que influenciam diretamente os custos de novos projetos de infraestrutura de bases e terminais de distribuição de combustíveis.</p>
                        </div>
                    </div>
                                
                    <div class="cardBlock">
                        <div class="maskImage">
                            <img src="https://res.cloudinary.com/supplychain-/image/upload/v1660846801/supply%20chain/images/terminal-4.0_x556w3.png" data-src="https://res.cloudinary.com/supplychain-/image/upload/v1660846801/supply%20chain/images/terminal-4.0_x556w3.png" alt="Terminais 4.0">
                        </div>
    
                        <div class="cardInformation">
                            <h2>Terminais 4.0</h2>
                            <p>Startups que desenvolvam produtos com novas tecnologias e processos de automação que podem ser aplicados nos terminais da Raízen, para que sejam como a indústria 4.0: eficientes e automatizados.</p>
                        </div>
                    </div>
                                
                    <div class="cardBlock">
                        <div class="maskImage">
                            <img src="https://res.cloudinary.com/supplychain-/image/upload/v1660846798/supply%20chain/images/logistica_m3r7fi.png" data-src="https://res.cloudinary.com/supplychain-/image/upload/v1660846798/supply%20chain/images/logistica_m3r7fi.png" alt="pulse-image">
                        </div>
    
                        <div class="cardInformation">
                            <h2>Eficiência logística e Mobilidade</h2>
                            <p>Soluções envolvendo mobilidade urbana e gerenciamento de informações sobre frotas, ativos, transportes e cargas, garantindo que os produtos produzidos pela Raízen cheguem até o consumidor final com agilidade e eficiência.</p>
                        </div>
                    </div>
                                
                    <div class="cardBlock">
                        <div class="maskImage">
                            <img src="https://res.cloudinary.com/supplychain-/image/upload/v1660846802/supply%20chain/images/treinamento_m85nx8.png" data-src="https://res.cloudinary.com/supplychain-/image/upload/v1660846802/supply%20chain/images/treinamento_m85nx8.png" alt="Gestão de Treinamento">
                        </div>
    
                        <div class="cardInformation">
                            <h2>Gestão de Treinamento</h2>
                            <p>Projetos para capacitação e treinamento de motoristas das frotas Raízen, com foco em educação corporativa e prática dos profissionais envolvidos, para que estejam sempre dentro da conformidade legal necessária.</p>
                        </div>
                    </div>
                                
                    <div class="cardBlock">
                        <div class="maskImage">
                            <img src="https://res.cloudinary.com/supplychain-/image/upload/v1660846804/supply%20chain/images/conhecimento-icon_tq5g7w.png" data-src="https://res.cloudinary.com/supplychain-/image/upload/v1660846804/supply%20chain/images/conhecimento-icon_tq5g7w.png" alt="Gestão do Conhecimento">
                        </div>
    
                        <div class="cardInformation">
                            <h2>Gestão do Conhecimento</h2>
                            <p>Ferramenta que suporte a gestão do conhecimento de forma centralizada, que facilite a busca dos históricos, mostre as conexões e sinergias entre os assuntos tratados pelas áreas e que otimize a transferência de conhecimento entre as diversas áreas da empresa.</p>
                        </div>
                    </div>
                </div>
            </div>
            <span class="graphics_green"></span>

            <a href="https://app.pipefy.com/public/form/yVidpha_" target="_blank" class="primaryButton">Quero inscrever minha startup</a>
        </section>

        <section id="oportunidades-e-regulamento" class="purpleContainer">
            <h2 class="textCenter">MOTIVOS PARA SE INSCREVER NA CHAMADA DE SUPPLY CHAIN</h2>

            <div class="containerBlockInformation">
                <div class="blockInformation">
                    <span class="iconInformation user_icon">
                    </span>
                    Ter sua solução, software ou produto impactando a vida de <strong>mais de 50 milhões de clientes da Raízen.</strong>
                </div>

                <div class="blockInformation">
                    <span class="iconInformation flag_icon">
                    </span>
                    Oportunidade de atuar em mais de 70 bases de distribuição e abastecimento com a marca Shell no <strong>Brasil</strong> e na <strong>Argentina</strong>.
                </div>

                <div class="blockInformation">
                    <span class="iconInformation globe_icon">
                    </span>
                    Desenvolver um projeto-piloto e movimentar a economia em uma empresa que é referência global e conta com um vasto portfólio de produtos.
                </div>

                <div class="blockInformation">
                    <span class="iconInformation users_icon">
                    </span>
                    Simplificar o dia a dia de <strong>mais de 40 mil funcionários da Raízen</strong>.
                </div>

                <div class="blockInformation">
                    <span class="iconInformation zap_icon">
                    </span>
                    Fazer parte da transformação mundial em <strong>geração de energia de valor</strong>.
                </div>
            </div>

            <span class="graphics_arrow_right"></span>
        </section>

        <section class="secundaryContent">
            <div class="regulationWrapper">
                <h2>PARA CONFERIR O REGULAMENTO COMPLETO</h2>
                <a href="https://res.cloudinary.com/supplychain-/image/upload/v1660848848/supply%20chain/download/Chamada_de_Supply_Chain_-_Regulamento_final_k04jad.pdf" class="secundaryButton" download="Regulamento" target="_blank">clique aqui</a>
            </div>
            <span class="lineVertical_green"></span>
        </section>

        <section id="cronograma" class="purpleContainer">
            <h2 class="textCenter">CRONOGRAMA</h2>

            <ul class="schedule">
                <li>
                    <span class="date">22/08</span>
                    <h3>Lançamento da Chamada de Supply Chain</h3>
                    <p>Prospecção de interessados em fazer parte da transformação da Raízen.</p>
                </li>
                
                <li>
                    <span class="date">22/08 a 11/09</span>
                    <h3>Inscrição</h3>
                    <p>Período de inscrição de startups e empresas que se identificam com a proposta da Chamada.</p>
                </li>

                <li>
                    <span class="date">19/09 a 23/09</span>
                    <h3>Avaliação das propostas</h3>
                    <p>Enviadas dentro do prazo e conforme o regulamento.</p>
                </li>

                <li>
                    <span class="date">26/09</span>
                    <h3>Divulgação dos selecionados</h3>
                    <p>Após avaliação dos inscritos e necessidades da Raízen.</p>
                </li>
				
                <li>
                    <span class="date">03/10 a 14/10</span>
                    <h3>Pitch Day</h3>
                    <p>Apresentação presencial dos projetos selecionados.</p>
                </li>
				
                <li>
                    <span class="date">27/10</span>
                    <h3>Divulgação dos finalistas</h3>
                    <p>A partir das apresentações da etapa anterior.</p>
                </li>
				
            </ul>
        </section>

        <section class="secundaryContent graphics">
                <h2>ENVIE SUA PROPOSTA</h2>
                <p class="textCenter">Para transformar a área de Supply Chain e o futuro da energia com a Raízen</p>
                <a href="https://app.pipefy.com/public/form/yVidpha_" target="_blank" class="primaryButton">Quero me inscrever</a>
        </section>
    </main>

    <footer>
        <div class="containerWrapper">
            <a href="https://pulsehub.com.br/" class="logo">#raizentech</a>
    
            <div class="socialInformations">
                <a class="emailtext" href="mailto:contato@pulsehub.com.br" target="_blank">contato@pulsehub.com.br</a>
                <div class="socialLinks">
                    <a href="https://www.linkedin.com/company/pulsehubinovacao/" target="_blank" class="social-linkedin"></a>
                    <a href="https://www.youtube.com/c/PulseHubdeInova%C3%A7%C3%A3o" target="_blank" class="social-youtube"></a>
                    <a href="https://www.instagram.com/pulsehub/" target="_blank" class="social-instagram"></a>
                </div>
            </div>
        </div>

    </footer>
    <script src="https://res.cloudinary.com/supplychain-/raw/upload/v1661175856/supply%20chain/js/inline_vta9ny.js"></script>